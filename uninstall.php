<?php

if (!defined('WP_UNINSTALL_PLUGIN')) {
    exit;
}

// When the plugin is uninstalled, brush away all footprints; there shall be no trace.
global $wpdb;

// Remove all of our 'gialdeb_addons' post meta
$wpdb->delete(

// Table
    "{$wpdb->prefix}postmeta",

    // Where
    array('meta_key' => 'gialdeb_addons')
);

// Remove our 'gialdeb_addons' option
delete_option('gialdeb_addons_image');

// Remove our 'gialdeb_addons_applied' option
delete_option('gialdeb_addons_applied_image');

// Remove our DB version option (legacy)
delete_option('gialdeb_addons_db_version_image');
