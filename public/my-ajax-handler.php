<?php

class MyAjaxHandler
{

    public function __construct()
    {
//        add_action('init', array($this, 'register'));
        add_action('wp_enqueue_scripts', array($this, 'js'));
//        add_action('wp_ajax_my_ajax_handler', array($this, 'ajax'));
//        add_action('wp_ajax_nopriv_my_ajax_handler', array($this, 'ajax'));
    }

    public function js() : void
    {
        wp_register_script('custom-addons-js', Gialdeb_URI . 'js/public/custom-addons.js', array(),null, true);

        // creo un array per gestire queste variabili in javascript
        $vars = array(
            'ajaxurl' => admin_url('admin-ajax.php'),
            'nonce' => wp_create_nonce('my-ajax-handler-nonce')
        );

        wp_enqueue_script('custom-addons-js');

        // passo le variabiali a javascript
        wp_localize_script('custom-addons-js', 'my_ajax_handler', $vars);
    }

}

function save_custom_form()
{
    $posts = $_POST;
    $files = $_FILES;

    // directory upload
    if ( ! is_dir( wp_upload_dir()['basedir'] . '/gialdeb' ) ) {
        wp_mkdir_p( wp_upload_dir()['basedir'] . '/gialdeb' );
    }

    $uploaddir = wp_upload_dir()['basedir'] . '/gialdeb/';
    //var_dump($uploaddir);
    chmod( $uploaddir, 0777 );

    $datas = sanitize_input($posts);

    foreach ($files as $key => $file){

        if(strlen($file['name']) > 0 && strlen($file['tmp_name']) > 0){
            // fileVideoDemo -> mp3 demo
            // textUpload -> testo migliore
            // imageUpload -> foto profilo
            // cvUpload -> curriculum
            $tmp = str_replace(' ','-',$uploaddir . $posts['name']."-".$posts['surname']."-".date("Y-m-d-h-i-s"). '-' . basename($file['name']));
            $pathToDb = wp_upload_dir()['baseurl'].'/gialdeb/'.str_replace(' ','-',$posts['name']."-".$posts['surname']."-".date("Y-m-d-h-i-s"). '-' . basename($file['name']));

            $raw_file_name = $file['tmp_name'];
            $result = move_uploaded_file($raw_file_name, $tmp);
            //var_dump($result);
            chmod($tmp, 0777);
            //var_dump($result);
            // scrivere un log del risultato del salvataggio
            $datas[$key] = $pathToDb;
        }
    }

    echo json_encode(insertInputInTable($datas), true);
    die();
}

add_action('wp_ajax_save_custom_form', 'save_custom_form');
add_action('wp_ajax_nopriv_save_custom_form', 'save_custom_form');

// non utilizzata, lasciata la gestione al plugin
function check_validation_captcha($token)
{

    $url = "https://www.google.com/recaptcha/api/siteverify";
    $secret = "6LdWZlkeAAAAAF8UG5bf5zNSzbfjNORDesxCKRsZ";
    $post = "secret=".$secret."&response=".$token;

    //var_dump(json_encode($post));

//    $ch = curl_init();
//    curl_setopt($ch, CURLOPT_URL,$url);
//    curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
//    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
//
//    $server_output = curl_exec($ch);
//    curl_close ($ch);
    $server_output = wp_remote_post($url,[
        'method' => 'POST',
        'headers' => [
            'Content-type: application/x-www-form-urlencoded',
        ],
        'body' => $post,
    ]);

    //var_dump($server_output['body']);

    return $server_output;
}

function sanitize_input($input) : array
{

    $sanitized = [];
    /* Campi obbligatori */

    $sanitized['name'] = sanitize_text_field($input['name']);
    $sanitized['surname'] = sanitize_text_field($input['surname']);
    $sanitized['date'] = sanitize_text_field($input['date']);
    $sanitized['bornIn'] = sanitize_text_field($input['bornIn']);
    $sanitized['city'] = sanitize_text_field($input['city']);
    $sanitized['street'] = sanitize_text_field($input['street']);
    $sanitized['cap'] = sanitize_text_field($input['cap']);
    $sanitized['province'] = sanitize_text_field($input['province']);
    $sanitized['region'] = sanitize_text_field($input['region']);
    $sanitized['phone'] = sanitize_text_field($input['phone']);
    $sanitized['email'] = sanitize_email($input['email']);
    $sanitized['ip'] = $input['ip'];
    $sanitized['user_agent'] = $input['user_agent'];

    // nome d'arte
    $sanitized['stageName'] = sanitize_text_field($input['stageName']);

    // 1 miglior testo 2 miglior compositore 3 miglior cover
    $sanitized['competition'] = $input['competition'];

    /* fine campi obbligatori */

    //demo o video in diretta
    $sanitized['compositor'] = $input['compositor'];

    // link del video
    $sanitized['linkVideoDemo'] = sanitize_url($input['linkVideoDemo']);

    return $sanitized;

}

function insertInputInTable($datas)
{

    try {
        global $wpdb;

        $table = $wpdb->prefix . 'gialdeb_form_submissions';

        $wpdb->insert( $table,[
            "id" => NULL,
            "name" => $datas['name'],
            "surname" => $datas['surname'],
            "date_of_birth" => $datas['date'],
            "born_in" => $datas['bornIn'],
            "city" => $datas['city'],
            "street" => $datas['street'],
            "cap" => $datas['cap'],
            "province" => $datas['province'],
            "region" => $datas['region'],
            "phone" => $datas['phone'],
            "email" => $datas['email'],
            "stage_name" => $datas['stageName'],
            "competition" => $datas['competition'],
            "compositor" => $datas['compositor'] ?? null,
            "link_video" => $datas['linkVideoDemo'] ?? null,
            "path_file_audio" => $datas['fileVideoDemo'] ?? null,
            "path_file_cv" => $datas['cvUpload'] ?? null,
            "path_personal_photo" => $datas['imageUpload'] ?? null,
            "path_document_text" => $datas['textUpload'] ?? null,
            "user_agent" => $datas['user_agent'] ?? null,
            "ip_address" => $datas['ip'] ?? null,
            "submitted_at" => date('Y-m-d H:i:s'),
        ]);


	    if(!empty($wpdb->last_error)){
		    return [
			    'code'    => 500,
			    'message' => $wpdb->last_error,
			    'return'  => 0
		    ];
	    }

	    return 1;

    } catch (\Exception $e){
        return $e->getMessage();
    }

}


new MyAjaxHandler;