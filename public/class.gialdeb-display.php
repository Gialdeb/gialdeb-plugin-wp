<?php

if (!class_exists('Gialdeb_Display')) {

    class Gialdeb_Display
    {

        public function __construct()
        {
            add_action('admin_init', array($this, 'init'));
        }

        public function init()
        {
            // Add plugin on Front-End
            // todo:: Search Filter WP
            add_filter('', array($this, ''), 5);

            // Allow the use of shortcodes within the content
            // todo:: Add a Shortcode
            add_filter('gialdeb_content', 'do_shortcode');

        }

    }

    new Gialdeb_Display();

}
