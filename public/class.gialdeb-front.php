<?php

if (!class_exists('Gialdeb_Front')) {

    class Gialdeb_Front
    {


        public function __construct()
        {
            add_action('init', array($this, 'init'));
        }


        public function init()
        {

            // Public JS / CSS files
            add_action('wp_enqueue_scripts', array($this, 'enqueue_scripts_and_styles_front_end'));

        }

        /**
         * Enqueue all the required scripts and styles on the appropriate pages public folder
         *
         * @param string | $hook | The current page slug optional
         */
        public function enqueue_scripts_and_styles_front_end(string $hook): void
        {

            // Public Assets CSS
            wp_enqueue_style('custom-css', Gialdeb_URI . 'css/public/custom.css');
            wp_enqueue_style('toastr-css', Gialdeb_URI . 'css/toastr.min.css');

            // Public Assets JS
            wp_enqueue_script('just-validate', "https://unpkg.com/just-validate@latest/dist/just-validate.production.min.js", array(), '1', true);
            wp_enqueue_script('toastr-js', Gialdeb_URI . 'js/toastr.min.js' , array(), '1',true);
            wp_enqueue_script('jquery','https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js',array(),'1',true);
            wp_enqueue_script('jquery-block','https://cdnjs.cloudflare.com/ajax/libs/jquery.blockUI/2.70/jquery.blockUI.min.js',array(),'1',true);


        }


    }
    new Gialdeb_Front();
}