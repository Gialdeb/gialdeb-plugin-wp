/**
* "animazioni".
* */

/**
 * 1 miglior testo
 * 2 miglior compositore
 * 3 miglior cover
 */


jQuery('#inputCompetition').change(function(e) {

    const val = jQuery(this).val();

    switch (val){

        case '1' :
            jQuery('#blockBestText').show();
            // jQuery('#blockCompositor').hide();
            jQuery('#blockDemo').hide();
            jQuery('#inputCompositor').val("-1");
            jQuery('#blockVideoLive').hide();
            break;
        case '2' :
            jQuery('#blockBestText').hide();
            jQuery('#blockDemo').show();
            break;
        case '3':
            jQuery('#blockBestText').hide();
            jQuery('#blockDemo').show();
            break;
        default :
            //error
            break;
    }
    customValidationForm();

});

/**
 * 1 demo
 * 2 video in diretta
 */

// jQuery('#inputCompositor').change(function() {
//     const val = jQuery(this).val();
//
//     switch (val){
//
//         case '1' :
//             jQuery('#blockDemo').show();
//             jQuery('#blockVideoLive').hide();
//             break;
//         case '2' :
//             jQuery('#blockDemo').hide();
//             jQuery('#blockVideoLive').show();
//             break;
//         default :
//             //error
//             break;
//     }
//
// });


jQuery(function (){
    customValidationForm();
})


/**
 * Controlli e chiamata asincrona per invio dati.
 */

async function sendForm(){

    const data = await jQuery.getJSON('https://ipapi.co/json/', function(data) {
        return data;
    });

    let form = jQuery('#custom_form')[0];
    let varform = new FormData(form);

    varform.append('user_agent',window.navigator.userAgent);
    varform.append('ip',data.ip);
    varform.append("action", "save_custom_form");


    jQuery.ajax({
        type: "POST",
        url: my_ajax_handler.ajaxurl,
        dataType: "JSON",
        data: varform,
        processData: false,
        contentType: false,
        cache: false,
        crossDomain:true,
        beforeSend: function (){

            jQuery.blockUI({ message: '<div> <p>Attendi il caricamento...</p> </div>' });
        },
        success: function(response){

            // console.log("succes:"+response);

            if(response === 1){

                toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": false,
                    "positionClass": "toast-bottom-center",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                }

                toastr.success("Grazie per averci inviato le sue informazioni", "Successo")
                jQuery('#submit_custom_form').attr('disabled', false);
                jQuery('#custom_form').trigger("reset");
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(thrownError);
        }

    });

    // unblock when ajax activity stops
    jQuery(document).ajaxStop(jQuery.unblockUI);
}


function customValidationForm(){

    var myEle = document.getElementById("custom_form");
    if(!myEle) return;


    const validation = new JustValidate('#custom_form', {
        errorFieldCssClass: 'invalid-feedback',
        errorLabelStyle: {
            fontSize: '14px',
            color: '#dc3545',
        },
        successFieldCssClass: 'is-valid',
        successLabelStyle: {
            fontSize: '14px',
            color: '#20b418',
        },
        focusInvalidField: true,
        lockForm: true,
    });


    const textUpload = jQuery("#inputTextUpload").is(':visible');

    validation
        .addField('#inputName', [
            {
                rule: 'required',
                errorMessage: 'Questo campo è obbligatorio',
            },
        ])
        .addField('#inputSurname', [
            {
                rule: 'required',
                errorMessage: 'Questo campo è obbligatorio',
            },
        ])
        .addField('#inputDate', [
            {
                rule: 'required',
                errorMessage: 'Questo campo è obbligatorio',
            },
        ])
        .addField('#inputBornIn', [
            {
                rule: 'required',
                errorMessage: 'Questo campo è obbligatorio',
            },
        ])
        .addField('#inputCity', [
            {
                rule: 'required',
                errorMessage: 'Questo campo è obbligatorio',
            },
        ])
        .addField('#inputStreet', [
            {
                rule: 'required',
                errorMessage: 'Questo campo è obbligatorio',
            },
        ])
        .addField('#inputCAP', [
            {
                rule: 'required',
                errorMessage: 'Questo campo è obbligatorio',
            },
            {
                rule: 'number',
                errorMessage: 'Questo campo deve essere numerico',
            },
        ])
        .addField('#inputProvince', [
            {
                rule: 'required',
                errorMessage: 'Questo campo è obbligatorio',
            },
        ])
        .addField('#inputRegion', [
            {
                rule: 'required',
                errorMessage: 'Questo campo è obbligatorio',
            },
        ])
        .addField('#inputPhone', [
            {
                rule: 'required',
                errorMessage: 'Questo campo è obbligatorio',
            },
            {
                rule: 'number',
                errorMessage: 'Questo campo deve essere numerico',
            },
        ])
        .addField('#inputEmail', [
            {
                rule: 'required',
                errorMessage: 'Questo campo è obbligatorio',
            },
            {
                rule: 'email',
                errorMessage: 'Email non valida',
            },
        ])
        .addField('#inputStageName', [
            {
                rule: 'required',
                errorMessage: 'Questo campo è obbligatorio',
            },
        ])
        .addField('#inputImageUpload', [
            {
                    rule: 'minFilesCount',
                    value: 1,
                    errorMessage: 'Inserisci almeno un file',
            },
            {
                    rule: 'maxFilesCount',
                    value: 1,
                    errorMessage: 'Inserisci almeno un file',
            },
            {
                rule: 'files',
                value: {
                    files: {
                        types: ['image/png','image/jpg','image/jpeg'],
                        extensions: ['png','jpg','jpeg'],
                    },
                },
                errorMessage: 'Inserisci un formato jpeg/png/jpg',
            },
        ])
        .addField('#inputCVupload', [
            {
                rule: 'minFilesCount',
                value: 1,
                errorMessage: 'Inserisci almeno un file',
            },
            {
                rule: 'maxFilesCount',
                value: 1,
                errorMessage: 'Inserisci almeno un file',
            },
            {
                rule: 'files',
                value: {
                    files: {
                        // types: ['document/doc','document/docx','document/pdf'],
                        extensions: ['doc','docx','pdf'],
                    },
                },
                errorMessage: 'Inserisci un formato doc/docx/pdf',
            },
        ])
        .addField('#checkboxContract', [
            {
                rule: 'required',
                errorMessage: 'Questo campo è obbligatorio',
            },
        ])
        .onSuccess((ev) => {
            jQuery('#submit_custom_form').attr('disabled', true);
            ev.preventDefault();
            sendForm();
        });

        // if(textUpload){
        //
        //     validation.addField('#inputTextUpload', [
        //         {
        //             rule: 'minFilesCount',
        //             value: 1,
        //             errorMessage: 'Inserisci un file',
        //         },
        //         {
        //             rule: 'maxFilesCount',
        //             value: 1,
        //         },
        //         {
        //             rule: 'files',
        //             value: {
        //                 files: {
        //                     // types: ['document/doc','document/docx','document/pdf'],
        //                     extensions: ['doc','docx','pdf'],
        //                 },
        //             },
        //             errorMessage: 'Inserisci un formato doc/docx/pdf',
        //         },
        //     ])
        //
        // }else{
        //     //validation.removeField('#inputTextUpload');
        //
        // }



}

