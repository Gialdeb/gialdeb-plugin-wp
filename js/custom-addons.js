jQuery(document).ready(function () {


    jQuery('#submitajax').on('click', function (){
        alert('Call Ajax');
    });

    const table = new DataTable('#table_gialdeb', {
        url: my_ajax_back_handler.ajaxurl,
        language: {
            url: 'https://cdn.datatables.net/plug-ins/1.11.4/i18n/it_it.json'
        }
    });

    const tableArchive = new DataTable('#table_gialdeb_archive',{
        url: my_ajax_back_handler.ajaxurl,
        language: {
            url: 'https://cdn.datatables.net/plug-ins/1.11.4/i18n/it_it.json'
        }
    });



    // Remove data to table
    jQuery('#table_gialdeb').on('click', '.remove-gialdeb', function (){
        let id = jQuery(this).data('id');
        let row = jQuery(this).parents('tr');

        Swal.fire({
            title: 'Sei sicuro?',
            text: "Vuoi cancellare questo dato non sarà più recuperabile!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, elimina!'
        }).then((result) => {

            if (result.value == true) {

                jQuery.ajax({
                    type: "POST",
                    url: my_ajax_back_handler.ajaxurl,
                    data: {
                        action : 'remove_custom_form',
                        id:id
                    },
                    success: function(response){

                        if(response == 1){

                            table.row(row).remove().draw();

                            Swal.fire(
                                'Eliminato!',
                                'Eliminazione del campo avvenuta con successo.',
                                'success'
                            )
                            // toastr.options = {
                            //     "closeButton": true,
                            //     "debug": false,
                            //     "newestOnTop": false,
                            //     "progressBar": false,
                            //     "positionClass": "toast-bottom-center",
                            //     "preventDuplicates": false,
                            //     "onclick": null,
                            //     "showDuration": "300",
                            //     "hideDuration": "1000",
                            //     "timeOut": "5000",
                            //     "extendedTimeOut": "1000",
                            //     "showEasing": "swing",
                            //     "hideEasing": "linear",
                            //     "showMethod": "fadeIn",
                            //     "hideMethod": "fadeOut"
                            // }
                            //
                            // toastr.success("", "Successo")
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr.status);
                        console.log(thrownError);
                    }
                });
            }
        })

    });

    // Archive data
    jQuery(document).on('click', '.archive-gialdeb', function (){
        let id = jQuery(this).data('id');
        let archive = jQuery(this).data('archive');


        Swal.fire({
            title: 'Sei sicuro?',
            text: "Vuoi archiviare questo dato?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, archivia!'
        }).then((result) => {

            if (result.value == true) {

                jQuery.ajax({
                    url: my_ajax_back_handler.ajaxurl,
                    data: {
                        action : 'archive_custom_form',
                        id:id,
                        archive:archive,
                    },
                    success: function(response){

                        if(response == 1){

                            Swal.fire(
                                'Archiviato!',
                                'Archiviazione del campo avvenuta con successo.',
                                'success'
                            )

                            setTimeout(function (){
                                location.reload();
                            },300)
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr.status);
                        console.log(thrownError);
                    }
                });
            }
        })

    });

});