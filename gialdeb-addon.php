<?php
/**
 * Plugin Name: Gialdeb Custom Form Plugin
 * Plugin URI: https://www.giuseppealessandrodeblasio.it
 * Description: Plugin for Custom Form to WordPress
 * Author: Gialdeb
 * Author URI: https://www.giuseppealessandrodeblasio.it
 * Version: 1.0.0
 * Text Domain: giuseppealessandrodeblasio.it
 * Domain Path: languages/it
 *
 *
 * Copyright: (c) 2022 Gialdeb.
 *
 * License: GNU General Public License v3.0
 * License URI: http://www.gnu.org/licenses/gpl-3.0.html
 *
 */

// Exit if accessed directly.
if (!defined('ABSPATH')) exit;

// Must include plugin.php to use is_plugin_active().
require_once ABSPATH . 'wp-admin/includes/plugin.php';


// Call Class Action
new GialdebPlugin();


/**
 * Initialize the Custom addons Class Product.
 */
class GialdebPlugin
{

    /**
     * Gialdeb Addon constructor.
     */
    public function __construct()
    {

	    add_action('admin_init', array($this, 'init'));

        $this->define_constants();

        // Dependency Class
        require_once Gialdeb_Path . 'admin/class.gialdeb-addons.php';
        require_once Gialdeb_Path . 'admin/class.gialdeb-create-form-submissions-table.php';
        //require_once Gialdeb_Path . 'admin/server/server.php';


        // View Class
        require_once Gialdeb_Path . 'public/class.gialdeb-display.php';
        require_once Gialdeb_Path . 'public/class.gialdeb-front.php';

        // Ajax
        require_once Gialdeb_Path . 'public/my-ajax-handler.php';

        // Shortcode
        require_once Gialdeb_Path . 'public/function.form-shortcode.php';

    }

    /**
     * Define our constants.
     */
    private function define_constants(): void {

        /**
         * Define the page slug for our plugin's custom settings page in one central location.
         */
        if (!defined('Gialdeb_Settings_Page')) {
            define('Gialdeb_Settings_Page', 'gialdeb-settings');
        }

        /**
         * Define the plugin's version.
         */
        if (!defined('Gialdeb_Version')) {
            define('Gialdeb_Version', '1.0.0');
        }

        /**
         * Define the plugin's path.
         */
        if (!defined('Gialdeb_Path')) {
            define('Gialdeb_Path', plugin_dir_path(__FILE__));
        }

        /**
         * Define the plugin's URI.
         */
        if (!defined('Gialdeb_URI')) {
            define('Gialdeb_URI', plugin_dir_url(__FILE__));
        }

        /**
         * Define the page slug for our plugin's support page.
         */
        if (!defined('Gialdeb_Support_Page')) {
            define('Gialdeb_Support_Page', 'gialdeb-support');
        }

    }


    /**
     * Run our basic plugin setup.
     */
    public function init()
    {
        // Default WYSIWYG to 'visual'.
        add_filter('wp_default_editor', array($this, 'gialdeb_set_editor_to_visual'), 10, 1);

        // i18n.
        add_action('plugins_loaded', array($this, 'gialdeb_load_plugin_textdomain'));

        // Add settings link to plugin on plugins page.
//        add_filter('plugin_action_links_' . plugin_basename(__FILE__), array($this, 'add_plugin_action_links'), 10, 1);

	    // Add Database schema for form submission
	    add_action('plugins_loaded', array($this, 'gialdeb_action_db_submission_create'));
    }


    /**
     * Add a link to the settings page to the plugin's action links
     *
     * @param array $links An array of links passed from the plugin_action_links_{plugin_name} filter.
     *
     * @return array $links The $links array, with our saved tabs page appended.
     */
    public function add_plugin_action_links( array $links): array {
        $href = esc_url_raw(add_query_arg(array('page' => Gialdeb_Settings_Page), admin_url()));
        $links[] = '<a href="' . $href . '">Salva</a>';
        return $links;
    }

    /**
     * Register the textdomain for proper i18n / l10n.
     */
    public function gialdeb_load_plugin_textdomain(): void {
        load_plugin_textdomain(
            'gialdeb-addons',
            false,
            Gialdeb_Path . 'languages/'
        );
    }

	public function gialdeb_action_db_submission_create() {
		if ( is_plugin_active( Gialdeb_Path. '/gialdeb-addon.php' ) ) {
			return new Gialdeb_Create_Submissions_table;
		}
	}


    /**
     * Default the wp_editor to 'Visual' tab (this helps prevent errors with dynamically generating WYSIWYG)
     *
     * @param string $mode The current mode of the editor.
     *
     * @return string 'tinymce' || $mode
     */
    public function gialdeb_set_editor_to_visual( string $mode): string {
        global $post;

        // Only continue if we're on the products page.
        if (isset($post) && isset($post->post_type) && $post->post_type !== 'product') {
            return $mode;
        }

        // This is funky, but only default the editor when we don't have a post (and we're on the product page).
        // This a result of calling the wp_editor via AJAX - I think.
        if (!isset($post)) {
            return apply_filters('ge_default_editor_mode', 'tinymce');
        }

	    return $mode;
    }

}
