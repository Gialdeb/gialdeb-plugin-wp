<?php
/**
 * Plugin Name: Gialdeb Custom Form Plugin
 * Plugin URI: https://www.giuseppealessandrodeblasio.it
 * Description: Plugin for Custom Form to WordPress
 * Author: Gialdeb
 * Author URI: https://www.giuseppealessandrodeblasio.it
 * Version: 1.0.0
 * Text Domain: giuseppealessandrodeblasio.it
 * Domain Path: languages/it
 *
 *
 * Copyright: (c) 2022 Gialdeb.
 *
 * License: GNU General Public License v3.0
 * License URI: http://www.gnu.org/licenses/gpl-3.0.html
 *
 */

$path = $_SERVER['DOCUMENT_ROOT'] . "/";

include_once $path . '/wp-config.php';
include_once $path . '/wp-load.php';
include_once $path . '/wp-includes/wp-db.php';
include_once $path . '/wp-includes/pluggable.php';
include_once $path . '/wp-includes/post.php';

if (empty($_POST)) {

    $errors["errors"] = "Errore Complila tutti i campi";

    echo json_encode($errors);

    die();

} else {

    if (!empty($_POST['removeAddons'])) {

        (int)$id_addon = $_POST['removeAddons'];

        return delete_post_meta($id_addon, 'gialdeb_addons');

    } else {

        $response = $_POST['data'];
        $post_id = $_POST['post_id'];

        // Insert data into database
        update_post_meta($post_id, 'gialdeb_addons', $response);

        $success["success"] = "I dati sono stati inseriti correttamente";

        echo json_encode($success);
    }
}


