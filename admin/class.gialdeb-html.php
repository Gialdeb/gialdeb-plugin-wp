<?php

if (!class_exists('Gialdeb_HTML')) {
    class Gialdeb_HTML
    {

        public function __construct()
        {
            // empty...
        }

        /**
         * Creates all of the HTML required for tabs on the product edit screen
         *
         * @since 1.5
         */
        public function generate_html(): void {
            global $post;

            // Display HTML data to Dashboard
            echo $this->display_to_dashboard();


        }

        /**
         * Add View HTML Form Data to page
         *
         * @since 1.5
         *
         * return string HTML
         */
        protected function display_to_dashboard()
        {

            // Path plugin action
            $action = plugins_url('/server/server.php', __FILE__);;

            // retrieve current post
            $post_id = get_the_ID() ?? NULL;


            !get_post_meta($post_id, 'gialdeb_addons', true);

        }

        /**
         * Add how-to info HTML to page
         *
         * @return string HTML
         * @since 1.5
         *
         */
        protected function display_gialdeb_how_to()
        {
            $return_html = '';
            $return_html .= '<div class="gialdeb-how-to-info">';
            $return_html .= '<p class="gialdeb_how_to_info">' . __("For help using Litmos Plugin please visit our <a href='https://www.giuseppealessandrodeblasio.it' target='_blank'>Help</a>", 'gialdeb-addons') . '</p>';
            $return_html .= '</div>';
            $return_html .= '<div id="gialdeb-help-me-icon" class="dashicons dashicons-editor-help" title="' . __("Help Me!", 'gialdeb-addons') . '"></div>';

            return $return_html;
        }


        /* END HTML Functions */
    }
}

?>
