<?php

if (!class_exists('Gialdeb_Addons')) {

    class Gialdeb_Addons
    {


        public function __construct()
        {
            add_action('admin_init', array($this, 'init'));
            // Load Plugin on Admin Dashboard
            add_action('admin_menu', array($this, 'gialdeb_admin_menu'));
        }


	    public function init()
        {
            // todo:: if is necessary
            add_action('gialdeb_panels', array($this, 'gialdeb_panels'), 10, 1);
            do_action('gialdeb_panels');

            // Enqueue our JS / CSS files
            add_action('admin_enqueue_scripts', array($this, 'enqueue_scripts_and_styles'), 10, 1);

            // Public JS / CSS files
//            add_action('wp_enqueue_scripts', array($this, 'enqueue_scripts_and_styles_front_end'));

            // AJAX calls
            add_action('wp_ajax_gialdeb_get_wp_editor', array($this, 'gialdeb_get_wp_editor'));

        }

        /**
         * Show Menu to Dashboard Admin
         *
         */
        function gialdeb_admin_menu()
        {

            add_menu_page(
                'Gialdeb Plugin Dashboard',
                'Richieste Form',
                'manage_options',
                'gialdeb-menu-dash',
                array($this, 'gialdeb_admin_page'),
                'data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTc5MiIgaGVpZ2h0PSIxNzkyIiB2aWV3Qm94PSIwIDAgMTc5MiAxNzkyIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPjxwYXRoIGZpbGw9IiNhN2FhYWQiIGQ9Ik02NDMgOTExdjEyOGgtMjUydi0xMjhoMjUyem0wLTI1NXYxMjdoLTI1MnYtMTI3aDI1MnptNzU4IDUxMXYxMjhoLTM0MXYtMTI4aDM0MXptMC0yNTZ2MTI4aC02NzJ2LTEyOGg2NzJ6bTAtMjU1djEyN2gtNjcydi0xMjdoNjcyem0xMzUgODYwdi0xMjQwcTAtOC02LTE0dC0xNC02aC0zMmwtMzc4IDI1Ni0yMTAtMTcxLTIxMCAxNzEtMzc4LTI1NmgtMzJxLTggMC0xNCA2dC02IDE0djEyNDBxMCA4IDYgMTR0MTQgNmgxMjQwcTggMCAxNC02dDYtMTR6bS04NTUtMTExMGwxODUtMTUwaC00MDZ6bTQzMCAwbDIyMS0xNTBoLTQwNnptNTUzLTEzMHYxMjQwcTAgNjItNDMgMTA1dC0xMDUgNDNoLTEyNDBxLTYyIDAtMTA1LTQzdC00My0xMDV2LTEyNDBxMC02MiA0My0xMDV0MTA1LTQzaDEyNDBxNjIgMCAxMDUgNDN0NDMgMTA1eiIvPjwvc3ZnPg==',
                4
            );

        }


        public function gialdeb_admin_page(): void {

	        // breadcrumbs
	        echo $this->breadcrumbs();

			// title dash
            echo $this->dash_title();

			// todo: aggiungere la tabella delle richieste provenienti dal form
	        echo $this->displayTable();

            // Test Primary Button
//            echo '<input class="button-primary" type="submit" name="Save" value="Salva" id="submitajax" />';
        }

	    public function dash_title(  ) {
		    return "<h2>" . __('Dashboard - Richieste Form', 'gialdeb-menu') . "</h2>";
		}

	    public function breadcrumbs() {

			return <<<HTML
				<p class="breadcrumbs">
	            <span class="prefix"><?php echo __( 'Sei qui: ', 'gialdeb-menu-dash' ); ?></span>
		        <a href=""<?php echo admin_url( 'admin.php?page=gialdeb-menu-dash' ); ?>Richieste Sottomissioni Form</a> &rsaquo;
				<span class="current-crumb"><strong> Dashboard</strong></span>
				</p>
HTML;

		}

	    public function displayTable() {

		    global $wpdb;
		    $results = $wpdb->get_results("SELECT * FROM wp_gialdeb_form_submissions WHERE archive=0");
			echo '
			<div class="wrap nosubsub">	
			<h1 style="color: green">Nuovi Inserimenti</h1>
				<table id="table_gialdeb" class="table">
				    <thead>
				        <tr>
				            <th>Nome</th>
				            <th>Cognome</th>
				            <th>Data di Nascita</th>
				            <th>Nato/a</th>
				            <th>Città</th>
				            <th>Via</th>
				            <th>CAP</th>
				            <th>Provincia</th>
				            <th>Regione</th>
				            <th>Cellulare</th>
				            <th>Email</th>
				            <th>Nome d\'arte</th>
				            <th>Link Video</th>
				            <th>File Audio</th>
				            <th>File CV</th>
				            <th>Foto Personale</th>
				            <th>Documento Personale</th>
				            <th>Dispositivo usato</th>
				            <th>IP</th>
				            <th>Inviati il</th>
				            <th>Azione</th>
				        </tr>
				    </thead>
				    <tbody>
				    ';
				     foreach($results as $result):
				     echo '<tr>
				            <td>'.$result->name.'</td>
				            <td>'.$result->surname.'</td>
				            <td>'.$result->date_of_birth.'</td>
				            <td>'.$result->born_in.'</td>
				            <td>'.$result->city.'</td>
				            <td>'.$result->street.'</td>
				            <td>'.$result->cap.'</td>
				            <td>'.$result->province.'</td>
				            <td>'.$result->region.'</td>
				            <td>'.$result->phone.'</td>
				            <td>'.$result->email.'</td>
				            <td>'.$result->stage_name.'</td>
				            <td><a '.(!empty($result->link_video)? "href=". $result->link_video  : "href=javascript:;").'  download>'.(!empty($result->link_video)? "Link Video" : "" ).'</a></td>		
				            <td><a '.(!empty($result->path_file_audio)? "href=". $result->path_file_audio  : "href=javascript:;").'  download>'.(!empty($result->path_file_audio)? "Download Audio" : "" ).'</a></td>		  				
				            <td><a '.(!empty($result->path_file_cv)? "href=". $result->path_file_cv  : "href=javascript:;").'  download>'.(!empty($result->path_file_cv)? "Download CV" : "" ).'</a></td>		      				      
				            <td><a '.(!empty($result->path_personal_photo)? "href=". $result->path_personal_photo  : "href=javascript:;").'  download>'.(!empty($result->path_personal_photo)? "Download Foto Personale" : "" ).'</a></td>	      				      
				            <td><a '.(!empty($result->path_document_text)? "href= $result->path_document_text"  : "href=javascript:;").'  download>'.(!empty($result->path_document_text)? "Download Documento" : "" ).'</a></td>	      				         				      								   
				            <td>'.$result->user_agent.'</td>
				            <td>'.$result->ip_address.'</td>
				            <td>'. DateTime::createFromFormat("Y-m-d H:i:s", $result->submitted_at)->format('d-m-Y h:i:s') .'</td>
				            <td>
					            <div>
						            <button class="btn btn-danger remove-gialdeb dashicons-before dashicons-trash" id="delete-'.$result->id.'" data-id="'.$result->id.'" style="color: red; float: left"></button>
						            <button class="btn btn-warning archive-gialdeb dashicons-before dashicons-archive" id="archive-'.$result->id.'" data-id="'.$result->id.'" data-archive="'. (($result->archive == 0)? 1 : 0 ).'" style="float: right;position: relative;left: 10px;bottom: 26px; '. (($result->archive == 1)? "background-color:green;color:white" : "" ).'"></button>
								</div>
				            </td>			
				        </tr>';
				        endforeach;
				    echo '</tbody>
				</table>
				
			<h1 style="color: red">Archiviati</h1>';

		    $archives = $wpdb->get_results("SELECT * FROM wp_gialdeb_form_submissions WHERE archive=1");
		    echo '
			<div class="wrap nosubsub">
				<table id="table_gialdeb_archive" class="table">
				    <thead>
				        <tr>
				            <th>Nome</th>
				            <th>Cognome</th>
				            <th>Data di Nascita</th>
				            <th>Nato/a</th>
				            <th>Città</th>
				            <th>Via</th>
				            <th>CAP</th>
				            <th>Provincia</th>
				            <th>Regione</th>
				            <th>Cellulare</th>
				            <th>Email</th>
				            <th>Nome d\'arte</th>
				            <th>Link Video</th>
				            <th>File Audio</th>
				            <th>File CV</th>
				            <th>Foto Personale</th>
				            <th>Documento Personale</th>
				            <th>Dispositivo usato</th>
				            <th>IP</th>
				            <th>Inviati il</th>
				            <th>Azione</th>
				        </tr>
				    </thead>
				    <tbody>
				    ';
		    foreach($archives as $result):
			    echo '<tr>
				            <td>'.$result->name.'</td>
				            <td>'.$result->surname.'</td>
				            <td>'.$result->date_of_birth.'</td>
				            <td>'.$result->born_in.'</td>
				            <td>'.$result->city.'</td>
				            <td>'.$result->street.'</td>
				            <td>'.$result->cap.'</td>
				            <td>'.$result->province.'</td>
				            <td>'.$result->region.'</td>
				            <td>'.$result->phone.'</td>
				            <td>'.$result->email.'</td>
				            <td>'.$result->stage_name.'</td>
				            <td><a '.(!empty($result->link_video)? "href=". $result->link_video  : "href=javascript:;").'  download>'.(!empty($result->link_video)? "Link Video" : "" ).'</a></td>		
				            <td><a '.(!empty($result->path_file_audio)? "href=". $result->path_file_audio  : "href=javascript:;").'  download>'.(!empty($result->path_file_audio)? "Download Audio" : "" ).'</a></td>		  				
				            <td><a '.(!empty($result->path_file_cv)? "href=". $result->path_file_cv  : "href=javascript:;").'  download>'.(!empty($result->path_file_cv)? "Download CV" : "" ).'</a></td>		      				      
				            <td><a '.(!empty($result->path_personal_photo)? "href=". $result->path_personal_photo  : "href=javascript:;").'  download>'.(!empty($result->path_personal_photo)? "Download Foto Personale" : "" ).'</a></td>	      				      
				            <td><a '.(!empty($result->path_document_text)? "href= $result->path_document_text"  : "href=javascript:;").'  download>'.(!empty($result->path_document_text)? "Download Documento" : "" ).'</a></td>	      				         				      								   
				            <td>'.$result->user_agent.'</td>
				            <td>'.$result->ip_address.'</td>
				            <td>'. DateTime::createFromFormat("Y-m-d H:i:s", $result->submitted_at)->format('d-m-Y h:i:s') .'</td>
				            <td>
					            <div>
						            <button class="btn btn-warning archive-gialdeb dashicons-before dashicons-archive" id="archive-'.$result->id.'" data-id="'.$result->id.'" data-archive="'. (($result->archive == 0)? 1 : 0 ).'" style="relative; '. (($result->archive == 1)? "background-color:green;color:white" : "" ).'"></button>
								</div>
				            </td>			
				        </tr>';
		    endforeach;
		    echo '</tbody>
				</table>

				
		    </div>';
		}

		/**
         * Enqueue all the required scripts and styles on the appropriate pages
         *
         * @param string | $hook | The current page slug
         */
        public function enqueue_scripts_and_styles($hook = ''): void {

            // Gialdeb Style CSS
            wp_enqueue_style('fontawesome', Gialdeb_URI . 'css/font-awesome.min.css');
            wp_enqueue_style('toastr', Gialdeb_URI . 'css/toastr.min.css');
            wp_enqueue_style('vanilla-validate', Gialdeb_URI . 'css/justValidateTooltip.min.css');
            wp_enqueue_style('sweetalert2', Gialdeb_URI . 'css/sweetalert2.min.css');
            wp_enqueue_style('gialdeb-custom-css', Gialdeb_URI . 'css/custom.css');
            wp_enqueue_style('gialdeb-dataTables-css','https://cdn.datatables.net/1.11.4/css/jquery.dataTables.min.css');


            // Gialdeb JavaScript dependency
            wp_enqueue_script('script-just-validate', Gialdeb_URI . 'js/just-validate.min.js', null, null, true);
            wp_enqueue_script('toast-script', Gialdeb_URI . 'js/toastr.min.js', null, null, true);
            wp_enqueue_script('sweetalert-script', Gialdeb_URI . 'js/sweetalert2.min.js', null, null, true);
            wp_enqueue_script('gialdeb-custom-addons', Gialdeb_URI . 'js/custom-addons.js', array('jquery'), '20210211', true);
            wp_enqueue_script('gialdeb-dataTables-addons', 'https://cdn.datatables.net/1.11.4/js/jquery.dataTables.min.js', array('jquery'), '20210211', true);
            wp_enqueue_script('gialdeb-dataTables-translation', 'https://cdn.datatables.net/plug-ins/1.11.4/i18n/it_it.json', array('jquery'), '20210211', true);

	        // creo un array per gestire queste variabili in javascript
	        $vars = array(
		        'ajaxurl' => admin_url('admin-ajax.php'),
		        'nonce' => wp_create_nonce('my-ajax-handler-nonce')
	        );

	        // passo le variabiali a javascript
	        wp_localize_script('gialdeb-custom-addons', 'my_ajax_back_handler', $vars);


        }


        /**
         * Enqueue all the required scripts and styles on the appropriate pages public folder
         *
         * @param string | $hook | The current page slug optional
         */
        public function enqueue_scripts_and_styles_front_end( string $hook): void {

            // Public Assets CSS

            // Public Assets JS
//            wp_enqueue_script('custom-addons-js', Gialdeb_URI . 'js/public/custom-addons.js', array(), '20210211', true);

        }


        /**
         * Adds the panel to the Product Data postbox in the product interface
         */
        public function gialdeb_panels(): void {

            // Require & instantiate our HTML class
            require_once(Gialdeb_Path . 'admin/class.gialdeb-html.php');

            $HTML = new Gialdeb_HTML();

            // Call our function to generate the HTML
            $HTML->generate_html();
        }


        /**
         * Save the tabs to the database
         *
         * @param int  | $post_id | The ID of the post that has custom tabs
         * @param bool | $is_ajax_flag | A flag signifying whether we're calling this function from an AJAX call*
         * @return bool
         * @since 1.5
         *
         */
        protected function saveData($post_id, $is_ajax_flag)
        {
            $tab_data = array();

            return 'save_data';
        }

        /* AJAX Functions */

        /**
         * [AJAX] Return wp_editor HTML
         * (this is a bit funky, I know, but it's part of dynamically adding / removing the WYSIWYG )
         *
         * @param string $_POST ['textarea_id'] ID of the textarea that we're initializing wp_editor with
         * @param string $_POST ['tab_content'] content to pre-supply the text editor with
         * @return string wp_editor HTML
         * @since 1.5
         *
         */
        public function gialdeb_get_wp_editor()
        {

            // Verify nonce
            if (!check_ajax_referer('gialdeb_get_wp_editor_nonce', 'security_nonce', false)) {
                wp_send_json_error();
            }

            // Get & sanitize the $_POST var textarea_id
            $textarea_id = filter_var($_POST['textarea_id'], FILTER_SANITIZE_STRING);

            // Check if we have tab content
            $tab_content = isset($_POST['tab_content']) ? $_POST['tab_content'] : '';

            // Set up options
            $wp_editor_options = array(
                'textarea_name' => $textarea_id,
                'textarea_rows' => 8,
            );

            // Return wp_editor HTML
            wp_editor(stripslashes($tab_content), $textarea_id, $wp_editor_options);
            exit;
        }

        /**
         * [AJAX] Save all tabs for the current product
         *
         * @param string $_POST ['post_id']    | ID of the current product (post)
         * @param array $_POST ['product_tabs'] | array of all the tab data
         * @return object success w/ message || failure w/ message
         * @since 1.5
         *
         */
        public function gialdeb_save_data()
        {

            // Verify the nonce
            if (!check_ajax_referer('gialdeb_save__nonce', 'security_nonce', false)) {
                wp_send_json_error();
            }

            // Get our data id
            if (isset($_POST['post_id'])) { // example
                $post_id = filter_var($_POST['post_id'], FILTER_SANITIZE_NUMBER_INT);
            } else {

                // Fail gracefully...
                wp_send_json_error(array('message' => 'Could not find the product!'));
            }

            // Save our tabs!
            $success = $this->saveData($post_id, $is_ajax = true);

            if ($success === true) {
                wp_send_json_success(array('message' => 'Your tabs have been saved'));
            } else {
                wp_send_json_error(array('message' => 'Uh oh! Something went wrong with saving. Please try again.'));
            }
        }


    }

	/**
	 * @throws JsonException
	 */
	function archive_row_custom_form(): void {
		global $wpdb;
		$rowID = (int)$_GET['id'];
		$rowArchive = (int)$_GET['archive'];
		$table = $wpdb->prefix . 'gialdeb_form_submissions';
		$archive = $wpdb->update( $table, array('archive' => $rowArchive), array( 'id' => $rowID ) );

		if($archive) {
			echo json_encode( 1 , JSON_THROW_ON_ERROR | true );
		} else {
			echo json_encode(  0 , JSON_THROW_ON_ERROR | true );
		}
		die();
	}
	add_action('wp_ajax_archive_custom_form', 'archive_row_custom_form');
	add_action('wp_ajax_nopriv_archive_custom_form', 'archive_row_custom_form');

	/**
	 * @throws JsonException
	 */
	function remove_row_custom_form(): void {
		global $wpdb;
		$rowID = (int)$_POST['id'];
		$table = $wpdb->prefix . 'gialdeb_form_submissions';
		$delete = $wpdb->delete( $table, array( 'id' => $rowID ));

		if($delete) {
			echo json_encode( 1 , JSON_THROW_ON_ERROR | true );
		} else {
			echo json_encode(  0 , JSON_THROW_ON_ERROR | true );
		}
		die();
	}
	add_action('wp_ajax_remove_custom_form', 'remove_row_custom_form');
	add_action('wp_ajax_nopriv_remove_custom_form', 'remove_row_custom_form');

    new Gialdeb_Addons();
}

