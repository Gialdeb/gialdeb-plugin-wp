<div class="wrap gialdeb-admin-page-wrap">

    <!-- Title -->
    <h1>
        <span class="dashicons dashicons-exerpt-view"></span>
        <?php _e('Gialdeb | Support', 'gialdeb-addons'); ?>
    </h1>

    <!-- Support-content Hook -->
    <?php do_action('gialdeb-support-page'); ?>

</div>
