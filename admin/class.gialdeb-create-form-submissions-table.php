<?php

if (!class_exists('Gialdeb_Create_Submissions_table')) {

	class Gialdeb_Create_Submissions_table
	{

		public function __construct()
		{
			add_action('admin_init', array($this, 'createTableOnActivationPlugin'));
		}


		/**
		 * @return void
		 */
		public function createTableOnActivationPlugin(): void {
			/** @var wpdb */
			global $wpdb;

			// create table for storing submissions
			$table = $wpdb->prefix . 'gialdeb_form_submissions';
			$wpdb->query(
				"CREATE TABLE IF NOT EXISTS {$table}(
				        `id` INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
				        `name` VARCHAR(255) NOT NULL,                        
				        `surname` VARCHAR(255) NOT NULL,
                        `date_of_birth` VARCHAR(255) NOT NULL, 
                        `born_in` VARCHAR (255) NOT NULL,
                        `city` VARCHAR (255) NOT NULL,
                        `street` VARCHAR (255) NOT NULL,
                        `cap` VARCHAR (255) NOT NULL,
                        `province` VARCHAR (255) NOT NULL,
                        `region` VARCHAR (255) NOT NULL,
                        `phone` VARCHAR (255) NOT NULL,
                        `email` VARCHAR(255) NOT NULL,
                        `stage_name` VARCHAR (255) NOT NULL,
                        `competition` VARCHAR (1) NOT NULL,
                        `compositor` VARCHAR (1),
                        `link_video` TEXT,      
                        `path_file_audio` TEXT,
                        `path_file_cv` TEXT,
                        `path_personal_photo` TEXT,
                        `path_document_text` TEXT,
				        `user_agent` TEXT NULL,
				        `ip_address` VARCHAR(255) NULL,
    					`archive` tinyint(1) NOT NULL DEFAULT '0',
				        `submitted_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
				) ENGINE=INNODB CHARACTER SET={$wpdb->charset};"
			);
		}
	}

	new Gialdeb_Create_Submissions_table();
}
